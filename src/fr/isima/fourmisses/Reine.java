package fr.isima.fourmisses;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;

public class Reine extends Fourmie {
	
	private double tempsPonte;
	
	private void pondre() {
		if(tempsReference + (tempsPonte*1000) < System.currentTimeMillis() && maColonie.getNourriture()>=2) {
			
			tempsReference = System.currentTimeMillis();
			
			maColonie.addNourriture(-2);
			maColonie.setLarves(maColonie.getLarves()+1);
		}
	}
	
	@Override
	protected void metier() {
		switch(metier) {
		case 0:
			pondre();
			break;
		default:
			pondre();
		}
	}
	
	public Reine(Point p, Colonie col, Monde m) {
		super(p, col, m);
		this.force = 0;
		this.dureeVie = 3600;
		this.tempsPonte = 0.2;
		this.couleur = Color.magenta;
		this.taille = 5;
		box = new Rectangle.Double(p.x, p.y, taille, taille);
	}
	
	public void update() {
		super.update();
		if(System.currentTimeMillis() > dateNaissance + (dureeVie*1000))
			maColonie.notifMort(this);
	}
}
