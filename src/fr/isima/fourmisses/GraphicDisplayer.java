package fr.isima.fourmisses;

/*	***********************	*/
/*	The graphic displayer	*/
/*	***********************	*/

import java.awt.*;
import javax.swing.*;

public class GraphicDisplayer extends JPanel {
	private static final long serialVersionUID = 1L;
	private Monde monde;
	

	public GraphicDisplayer(Monde monde){
		super();
		this.monde = monde;
	}

	public void paintComponent(Graphics g)  {
		super.paintComponent(g);
		this.setBackground(Color.WHITE);

		for(Nourriture n : monde.getNourriture()) {
			g.setColor(Nourriture.couleur);
			g.fillRect(n.getX(), n.getY(), n.getWidth(), n.getHeight());
			g.setColor(Nourriture.couleurOdeur);
			g.fillRect(n.getLargeBox().x, n.getLargeBox().y, n.getLargeBox().width, n.getLargeBox().height);
		}
		
		for (Pheromone p : monde.getPheromones()) {
			g.setColor(Pheromone.couleur);
			g.fillRect(p.getX(), p.getY(), p.getWidth(), p.getHeight());
		}
		
		for(Colonie col : monde.getColonies()) {
			g.setColor(col.getCouleur());
			g.fillRect(col.getX(), col.getY(), col.getWidth(), col.getHeight());
			for(Fourmie fourmie : col.getFourmies()) {
				g.setColor(fourmie.getCouleur());
				g.fillRect((int)fourmie.getX(), (int)fourmie.getY(), fourmie.getTaille(), fourmie.getTaille());
				fourmie.metier();
			}
		}
		
	}
}