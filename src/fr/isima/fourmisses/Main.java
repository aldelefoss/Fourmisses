package fr.isima.fourmisses;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {

	static JFrame screen = new JFrame();
	static boolean go;
	static long time;
	
	public static void main(String[] args) {
		Monde monde = new Monde(1800, 800);
		GraphicDisplayer GD = new GraphicDisplayer(monde);
		go = new Boolean(false);
		
		JButton ouvriere = new JButton("Ouvriere");
		ouvriere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				monde.addOuvriere();
				GD.repaint();
		}
		});
		
		JButton run = new JButton("Run");
		run.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Runnable r = new Runnable() {
					public void run() {
						try {
							while (true) {
								monde.update();
								GD.repaint();
								Thread.sleep(40);
							}
						} catch (Exception ex) {
							System.out.println("Erreur RUN");
							ex.printStackTrace();
						}
					}
				};
				Thread me = new Thread(r);
				me.start();
			}
		});
		
		JButton Nourriture = new JButton("Nourriture");
		Nourriture.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				monde.spawnNourriture();
				}
			});
		
		JPanel boutonPanel = new JPanel();
		boutonPanel.add(ouvriere);
		boutonPanel.add(Nourriture);
		boutonPanel.add(run);
		
		screen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		screen.setSize(2200, 1000);
		screen.setLayout(new BorderLayout());
		screen.add(GD, BorderLayout.CENTER);
		screen.add(boutonPanel,BorderLayout.SOUTH);
		screen.setVisible(true);

	}

}
