package fr.isima.fourmisses;

import java.awt.Color;
import java.awt.Rectangle;

public class Nourriture {

	private Rectangle box;
	private Rectangle largeBox;
	private static int taille = 10;
	private static int portee = 10*taille;
	private int quantite;
	public static Color couleur = new Color(200,100,0,255);
	public static Color couleurOdeur = new Color(200,100,0,75);
	
	public Rectangle getBox() {
		return box;
	}
	public Rectangle getLargeBox() {
		return largeBox;
	}
	public int getX() {
		return box.x;
	}
	public int getY() {
		return box.y;
	}
	public int getWidth() {
		return box.width;
	}
	public int getHeight() {
		return box.height;
	}
	public int getQuantite() {
		return quantite;
	}
	public int diminuerQte(int qte) {
		int q;
		if (quantite-qte < 0) {
			q = quantite;
			quantite = 0;
		} else {
			q = qte;
			quantite -= qte;
		}
		return q;
	}
	
	public Nourriture(int x, int y) {
		box = new Rectangle(x,y,taille,taille);
		largeBox = new Rectangle(x-portee/2+taille/2, y-portee/2+taille/2, portee, portee);
		quantite = 120;
	}
}
