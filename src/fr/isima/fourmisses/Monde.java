package fr.isima.fourmisses;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Monde {
	private List<Colonie> colonies;
	private List<Nourriture> nourriture;
	private List<Pheromone> pheromones;
	private List<Pheromone> pheromonesMortes;
	private static double intervalleNourriture = 10;
	private static long beforeTemps=0;
	
	public List<Colonie> getColonies() {
		return colonies;
	}
	public List<Nourriture> getNourriture() {
		return nourriture;
	}
	public List<Pheromone> getPheromones() {
		return pheromones;
	}

	private void init() {
		
		int randomX = 800;//ThreadLocalRandom.current().nextInt(0, grid.length-100);
		int randomY = 400;//ThreadLocalRandom.current().nextInt(0, grid[0].length-100);
		Colonie col = new Colonie(randomX, randomY, this);
		col.addFourmie(0);
		colonies.add(col);
	}
	
	public Monde(int largeur, int hauteur) {
		colonies = new ArrayList<Colonie>();
		nourriture = new ArrayList<Nourriture>();
		pheromones = new ArrayList<Pheromone>();
		pheromonesMortes = new ArrayList<Pheromone>();
		init();
	}
	
	public void addOuvriere() {
		colonies.get(0).addFourmie(1);
	}
	
	public void notifPheromone(Pheromone p) {
		pheromonesMortes.add(p);
	}
	
	public void addPheromone(Pheromone p) {
		pheromones.add(p);
	}
	
	public void spawnNourriture() {			
		beforeTemps = System.currentTimeMillis();
		int randomX = ThreadLocalRandom.current().nextInt(400, 1200);
		int randomY = ThreadLocalRandom.current().nextInt(200, 800);
		Nourriture n = new Nourriture(randomX, randomY);
		nourriture.add(n);
	}
	
	public void update() {
		if(beforeTemps + (intervalleNourriture*1000) < System.currentTimeMillis()) {
			spawnNourriture();
		}
		Iterator<Nourriture> iter = nourriture.iterator();
		while (iter.hasNext()) {
		    Nourriture n = iter.next();
		    if(n.getQuantite() == 0) {
		    	iter.remove();
		    	nourriture.remove(n);
		    }
		}
		
		for(Colonie col : colonies) {
			col.update();
		}
		for(Pheromone p : pheromones) {
			p.update();
		}
		
		Iterator<Pheromone> iterp = pheromonesMortes.iterator();
		while (iterp.hasNext()) {
		    Pheromone p = iterp.next();
		    iterp.remove();
		    pheromones.remove(p);
		}
	}
}
