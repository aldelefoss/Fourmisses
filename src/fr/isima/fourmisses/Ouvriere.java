package fr.isima.fourmisses;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.concurrent.ThreadLocalRandom;

public class Ouvriere extends Fourmie {
	
	private int nourriture;
	private double tempsChangement;
	private static double tempsPheromones = 0.7 + (double)ThreadLocalRandom.current().nextInt(50, 100)*0.3/100;
	private static Color idleColor = Color.blue;
	private static Color goHomeColor = Color.green;
	private static Color workingColor = Color.black;
	
	private void se_balader() {
		if(maColonie.getNourriture() < 100) {
			metier = 1;
		} else {
			couleur = idleColor;
			if(tempsReference + (tempsChangement*1000) < System.currentTimeMillis()) {
				
				tempsReference = System.currentTimeMillis();
				
				if(distanceToColonie() > distColonieMax) {
					nouvelleTrajectoire((int)angleTo(maColonie.getBox()));
				} else {
					nouvelleTrajectoire(ThreadLocalRandom.current().nextInt(-45, 45));
				}
			}
			box.x += vitesse.x;
			box.y += vitesse.y;
		}
	}
	
	private void chercher_nourriture() {
		if(nourriture >= force || maColonie.getNourriture() >= 200) {
			metier = 0;
		} else {
			couleur = workingColor;
			if(tempsReference + (tempsChangement*1000) < System.currentTimeMillis()) {
				
				tempsReference = System.currentTimeMillis();
				
				if(distanceToColonie() > distColonieMax) {
					nouvelleTrajectoire((int)angleTo(maColonie.getBox()));
				} else {
					nouvelleTrajectoire(ThreadLocalRandom.current().nextInt(-45, 45));
				}
			}
			
			for(Pheromone p : monde.getPheromones()) {
				if(p.getBox().intersects(box)) {
					nouvelleTrajectoire((int)(p.getAngleToNourriture()-angle));
				}
			}
			
			for(Nourriture n : monde.getNourriture()) {
				if(n.getLargeBox().intersects(box)) {
					nouvelleTrajectoire((int)angleTo(n.getBox()));
					if(n.getBox().intersects(box)) {
						nourriture = n.diminuerQte(force);
						metier = 2;
					}
				}
			}
			
			box.x += vitesse.x;
			box.y += vitesse.y;
		}
			
	}
	
	private void ramener_nourriture() {
		if(nourriture == 0) {
			metier = 0;
		} else {
			if(tempsReference + (tempsPheromones*1000) < System.currentTimeMillis()) {
				tempsReference = System.currentTimeMillis();
				monde.addPheromone(new Pheromone((int)box.x, (int)box.y, (int)angleTo(maColonie.getBox())+(int)angle, monde));
			}
			couleur = goHomeColor;
			nouvelleTrajectoire((int)angleTo(maColonie.getBox()));
			if(maColonie.getBox().intersects(box)) {
				maColonie.addNourriture(nourriture);
				nourriture = 0;
				metier = 0;
			}
			box.x += vitesse.x;
			box.y += vitesse.y;
		}
	}
	
	@Override
	protected void metier(){
		switch(metier) {
		case 0:
			se_balader();
			break;
		case 1:
			chercher_nourriture();
			break;
		case 2:
			ramener_nourriture();
			break;
		default:
			se_balader();
		}
	}
	
	public Ouvriere(Point p, Colonie col, Monde m) {
		super(p, col, m);
		this.force = 16;
		this.dureeVie = 30;
		this.couleur = idleColor;
		this.taille = 5;
		this.vitesseMax = 1;
		this.tempsChangement = 2;
		this.distColonieMax = 400;
		this.nourriture = 0;
		box = new Rectangle.Double(p.x, p.y, taille, taille);
		nouvelleTrajectoire(ThreadLocalRandom.current().nextInt(0, 360));
	}
	
	public void update() {
		super.update();
		
		if(System.currentTimeMillis() > dateNaissance + (dureeVie*1000))
			maColonie.notifMort(this);
	}
}
