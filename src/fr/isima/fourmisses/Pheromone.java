package fr.isima.fourmisses;

import java.awt.Color;
import java.awt.Rectangle;

public class Pheromone {

	private Rectangle box;
	private static int taille = 10;
	public static Color couleur = new Color(200,80,200,100);
	private static int duree_vie = 15;
	private long timeNaissance;
	private Monde monde;
	private int angleToNourriture;
	
	public Rectangle getBox() {
		return box;
	}
	public int getX() {
		return box.x;
	}
	public int getY() {
		return box.y;
	}
	public int getWidth() {
		return box.width;
	}
	public int getHeight() {
		return box.height;
	}
	public int getAngleToNourriture() {
		return angleToNourriture;
	}
	
	public Pheromone(int x, int y, int angle, Monde m) {
		box = new Rectangle(x,y,taille,taille);
		timeNaissance = System.currentTimeMillis();
		angleToNourriture = angle+180;
		monde = m;
	}
	
	public void update() {
		if(System.currentTimeMillis() > timeNaissance + (Pheromone.duree_vie*1000)) {
			monde.notifPheromone(this);
		}
	}
}
