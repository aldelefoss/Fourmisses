package fr.isima.fourmisses;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Colonie {
	private static int compteur=9;
	private int id;
	private Rectangle box;
	private int taille_nurserie;
	private int taille_stockage;
	private int nourriture;
	private int larves;
	private List<Fourmie> fourmies;
	private List<Fourmie> fourmiesMortes;
	private Color couleur;
	private Monde monde;
	
	public Rectangle getBox() {
		return box;
	}
	public int getId() {
		return id;
	}
	public Color getCouleur() {
		return couleur;
	}
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
	public List<Fourmie> getFourmies() {
		return fourmies;
	}
	public int getX() {
		return box.x;
	}
	public int getY() {
		return box.y;
	}
	public int getWidth() {
		return box.width;
	}
	public int getHeight() {
		return box.height;
	}
	public int getTaille_nurserie() {
		return taille_nurserie;
	}
	public int getStockage() {
		return taille_stockage;
	}
	public int getNourriture() {
		return nourriture;
	}
	public void addNourriture(int nourriture) {
		this.nourriture += nourriture;
	}
	public int getLarves() {
		return larves;
	}
	public void setLarves(int larves) {
		this.larves = larves;
	}
	
	public void notifMort(Fourmie f) {
		fourmiesMortes.add(f);
	}
	
	public Point getCentre() {
		return new Point(box.x + (box.width/2), box.y + (box.height/2));
	}

	public void addFourmie(int type) {
		switch(type) {
		case 0:
			fourmies.add(new Reine(getCentre(), this, monde));
			break;
		case 1:
			fourmies.add(new Ouvriere(getCentre(), this, monde));
			break;
		default:
			System.out.println("Erreur: Mauvais type de fourmie.");
		}
	}
	
	public Colonie(int x, int y, Monde m) {
		box = new Rectangle(x,y,100,100);
		taille_nurserie = (box.width + box.height)/2;
		taille_stockage = (box.width + box.height)/2;
		fourmies = new ArrayList<Fourmie>();
		fourmiesMortes = new ArrayList<Fourmie>();
		couleur = Color.red;
		id = ++compteur;
		nourriture = 70;
		monde = m;
	}
	
	public void update() {
		for(Fourmie fou : fourmies) {
			fou.update();
		}
		for(int i=0; i<larves; i++)
			addFourmie(1);
		larves = 0;
		
		Iterator<Fourmie> iter = fourmiesMortes.iterator();
		while (iter.hasNext()) {
		    Fourmie f = iter.next();
		    iter.remove();
		    fourmies.remove(f);
		}
	}
}
