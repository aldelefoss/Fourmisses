package fr.isima.fourmisses;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

abstract public class Fourmie {
	protected double angle;
	protected int vitesseMax;
	protected int dureeVie;
	protected long dateNaissance;
	protected int force;
	protected Rectangle.Double box;
	protected int taille;
	protected Point2D.Double vitesse;
	protected Color couleur;
	protected long tempsReference;
	protected Colonie maColonie;
	protected int distColonieMax;
	protected int metier;
	protected Monde monde;

	public double getX() {
		return box.x;
	}
	public double getY() {
		return box.y;
	}
	public double getWidth() {
		return box.width;
	}
	public double getHeight() {
		return box.height;
	}
	public int getTaille() {
		return taille;
	}
	public Color getCouleur() {
		return couleur;
	}
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
	public int getDuree_vie() {
		return dureeVie;
	}
	public void setDuree_vie(int duree_vie) {
		this.dureeVie = duree_vie;
	}
	public int getForce() {
		return force;
	}
	public void setForce(int force) {
		this.force = force;
	}
	
	abstract protected void metier();
	
	public void nouvelleTrajectoire(int angleAjout){
		angle += angleAjout;
		angle = angle % 360;
		vitesse.x = Math.cos(angle*Math.PI/180) * vitesseMax;
		vitesse.y = Math.sin(angle*Math.PI/180) * vitesseMax;
	}
	
	public double distanceToColonie() {
		return (Math.sqrt(Math.pow(maColonie.getCentre().x-box.x,2) + Math.pow(maColonie.getCentre().y-box.y,2)));
	}
	
	public double angleTo(Rectangle rec) {
	
	double angleTo;
	try {
		angleTo = (180/Math.PI)*Math.atan((box.x-(rec.x+rec.width/2))/(box.y-(rec.y+rec.height/2)));
	} catch (ArithmeticException ae){
		if(box.x-(rec.x+rec.width/2) > 0)
			angleTo = 180;
		else
			angleTo = 0;
	}
	if((rec.y+rec.height/2)-box.y > 0)
		angleTo += 180;
	
	angleTo = -90 - angleTo;
	
	return angleTo - angle;
}
	
	public Fourmie(Point p, Colonie col, Monde m){
		monde = m;
		metier = 0;
		angle = 0;
		tempsReference = System.currentTimeMillis();
		dateNaissance = System.currentTimeMillis();
		vitesse = new Point2D.Double(0,0);
		maColonie = col;
	}
	
	public void update() {
		metier();
	}
}
